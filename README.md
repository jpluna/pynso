# PyNSO
This is a python library providing tools por minimizing functions. The package provides the following submodules:
- ## [tools](./doc/tools.md) 
- ## [Cutting Planes](./doc/cuttingPlane.ipynb) 
- ## [Proximal Bundle](./doc/proxBundle.ipynb) 
- ## [Level Bundle](./doc/levelREADME.md) 


All these methods return a `result` object that has some result information
- `x_best`: Best point found by the algorithm.
- `f_best`: Value at `x_best'
- `x_last`: Last point found by the algorithm.
- `f_best`: Value at `x_last'
- `deltak`: Gap value.
- `status`: 0 if algorithm ended with optimality.
- `nIter`: Number of iterations performed
- `time`: time in seconds used by the algorithm



Install
=======
via pip from gitlab:

```python
pip install git+https://gitlab.com/jpluna/pyuotools.git
pip install git+https://gitlab.com/jpluna/pynso.git
```
