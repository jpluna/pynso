###
import numpy as np
import pynso.testF as nsTestF
import pynso.proxBundle as pb
import pyuotools.tools as tt
###


bb = nsTestF.chainedLQ
bb = nsTestF.maxq
x0 = 2 * np.ones(2)
###

def myProxParamFun(state, changeProxCenter=False, context=None):
    if changeProxCenter: #serious step
        mu = 1
    else:
        mu = state.cIter + 1
    return mu


result = pb.minimize(x0, bb=bb, method='pnew',  globalLog='g.log', bundleMaxSize=10, maxIter=5, proxParamFun=myProxParamFun)
print(result)
tt.plotNsoLog('g.log')

