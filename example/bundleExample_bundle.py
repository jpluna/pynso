###
import numpy as np
import pynso.testF as nsTestF
import pynso.proxBundle as pb
import pyuotools.tools as tt
###


bb = nsTestF.chainedLQ
bb = nsTestF.maxq
x0 = 2 * np.ones(2)
###


result = pb.minimize(x0, bb=bb, method='pnew',  globalLog='g.log', bundleMaxSize=10, maxIter=5)
print(result)
tt.plotNsoLog('g.log')

result = pb.minimize(result.x, bb=bb, method='pnew',  globalLog='g2.log', bundleMaxSize=10, bundle=result.state.bundle)
print(result)
tt.plotNsoLog('g2.log')
