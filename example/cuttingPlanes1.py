###
import numpy as np
import pynso.testF as nsTestF
import pynso.cuttingPlane as cp
###

bb = nsTestF.chainedLQ
bb = nsTestF.maxq
x0 = 2 * np.ones(2)

# result = cp.cuttingPlane(x0, bb)
result = cp.minimize(x0, bb=bb)
print(result)

