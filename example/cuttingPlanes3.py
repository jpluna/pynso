###
import numpy as np
import pynso.testF as nsTestF
import pynso.cuttingPlane as cp
###

bb = nsTestF.maxq
bb = nsTestF.chainedLQ
x0 = 2 * np.ones(2)

result = cp.cuttingPlane(x0, bb, xLog='./x.log', globalLog='g.log', tol=1e-5, maxIter=50) 
print(result)


import pynso.tools
import matplotlib.pyplot as plt

pynso.tools.plot(bb=bb, files=[('./x.log', 'r')])
plt.show()
