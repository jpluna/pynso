###
import pyminit.ns.proxBundle as pb
import pyminit.ns.testF as testF
import numpy as np
###
bbList = [(testF.maxq, np.ones(2)), (testF.chainedLQ, np.ones(3))]
###
for bb, x0 in bbList[:1]:
# for cc in ['MAXQ']:
    res = pb.minimize(x0, bb=bb, xLog='./x.log', globalLog='./g.log',maxIter=50)
    print('x0:{}\n res --> {}'.format(x0, res))

###
