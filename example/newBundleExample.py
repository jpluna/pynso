import numpy as np
import pyminit.ns.testF as nsTestF
import pyminit.ns.proxBundle as pb

bb = nsTestF.maxq
bb = nsTestF.chainedLQ
x0 = 2 * np.ones(2)


# print(pb.minimize(x0,bb=bb,xLog='./x.log', globalLog='./g.log',bundleMaxSize=10, maxIter=50))
# print(pb.minimize(x0,bb=bb,xLog='./x.log', globalLog='./g.log',bundleMaxSize=10, maxIter=50, lb=5))
# print(pb.minimize(x0,bb=bb,xLog='./x.log', globalLog='./g.log', maxIter=50))
# result = pb.minimize(x0,bb=bb,xLog='./x.log', globalLog='./g.log', maxIter=50, method='python')
# result = pb.minimize(x0,bb=bb,xLog='./x.log', globalLog='./g.log', maxIter=50, method='C', solveProxM=None)
result = pb.minimize(x0,bb=bb,xLog='./x.log', globalLog='./g.log', maxIter=5, method='bmrio')
print(result)

print('x_best = {}'.format(result.x_best))
print('x_last = {}'.format(result.x_last))


