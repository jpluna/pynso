We use the level method for solving 

$`\begin{array}{rl}
\text{min}& f_1(x)\\
\text{s.t.} &  x\in X\\
 & f_j(x)\leq 0,\quad j>1\\
\end{array}`$

where the function $`f:\mathbb{R}^n\to\mathbb{R}^{m+1}`$, with $`m\geq 0`$, is convex  not necessarily differentiable.
The feasible set  $`X`$  is any compact set. It may even be defined considering integer variables.

```python
import pyminit.ns.levelBundle as nslb
nslb.minimize(x0, fun=None, grad=None, bb=None, bbContext=None,
        appSolver=None, appSolverContext=None, 
        bundle=[],
        L0=None,
        lb=None, ub=None,
        gamma=0.5,
        xLog=None, globalLog=None, 
        tol=1e-5, maxIter=10)

Parameters
----------
x0 : numpy array. 
	It represents the initial point at wich the algorithm will start.
L0: float (this value is required)
	A lower bound for the minimization problem
bb: Black Box funtion for `f=(f_1, f_2,..)`, where `f_1` is the objective function and `f_j`, j>1 are the explict constraints
bbContext: any object for probiding information to the bb.
appSolver: function. If None, the algorithm will assume there no explict constraints and will use an internal function. 
	appSolverDefault(x_best, f_best, x_last, f_last, levK, bundle, context=None)
		xk : numpy array
			Initial point  for the function
		levK: float
				Level value
		bundle: list [(b_id1, (x1, Fx1, Gx1)), (b_id2, (x2, Fx2, Gx2)),... ]: 
					b_idi: bundle cut id
					xi: numpy array
					Fi: float of array of same nature of `f(x)`
					Gi: 1D or 2D array  of same shape as  the jacobian of `f(x)`
    	return xkp, status
	The function finds and returns a point `xkp` (and a status =0) such that 
		- `xkp` belong to `X`
		- Fi + np.dot(G1, xkp -xi) <= [levK, 0,0..] for each (xi, Fi, Gi) in the bundle.
	in case such point does not exists, the function returns status=1
ub,lb: numpy arrays
	upper and lower bound for the feasible set. If appSovler is provided, ub and lb are negleted.
globalLog: string
	path for log file

Returns
-------
result class.
```
### Example: Minimization over box
```python
import pyminit.ns.levelBundle as nslb
import numpy as np

def f(x):
	return np.abs(x).sum() - 1

def s(x):
	sg = np.ones_like(x)
	sg[x<0] = -1.0
	return sg
x0 = np.ones(2)
L0 = -10
lb = -100
ub = 100
result = nslb.minimize(x0, fun=f, grad=s, L0=L0, lb=lb, ub=ub, maxIter=100)
print(result)
```
