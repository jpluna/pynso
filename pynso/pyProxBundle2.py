import numpy as np
import pynso.common as common
import time

class bundleClass(): 
    maxSize=None
    size=None 
    lastAbsId = None
    cut=None #cut[i] = [idcut, position]. ***IMPORTANT*** the columns [:,1] (second columns) always must contain a permutation of [0,1,..maxSize-1] 
    c1=None ## c1[position] 
    c0=None # c0[position] 
    alpha=None # alpha[position] mulitplier associated to the cut

    def __init__(self, n, maxSize=10, size=0):
        self.maxSize=maxSize
        self.size=size
        self.lastAbsId = 0
        self.cut = np.zeros((maxSize, 2),dtype=int)
        self.cut[:,1] = np.arange(self.maxSize)

        self.c1 = np.zeros((maxSize, n))
        self.c0 = np.zeros(maxSize)
        self.alpha = np.zeros(maxSize)

    def addCut(self, sg, c0, agregateCut=False):

        if (self.size >= self.maxSize): 
            print('bundleClass Warning: bundle is full. Oldest cut will be erased')
            aux = self.cut[0]
            self.cut[:-1,:-1] = self.cut[1:, 1:]
            self.cut[-1] = aux
            self.size -= 1

        newId = ( -1 if agregateCut else 1) * (self.lastAbsId + 1)
        self.lastAbsId +=1
        newEntry = self.size
        self.size += 1

        self.cut[newEntry, 0] = newId
        self.c1[newEntry, :] = sg
        self.c0[newEntry] = c0



        

class algothmState():
    fSerious=None
    xSerious=None
    sSerious=None
    fLast=None
    xLast=None
    sLast=None
    sa=None
    ea=None
    muS=None
    muN=None
    delta=None
    nIter=None
    def __init__(self, mu0):
        self.muS=mu0
        self.muN=mu0

#stop criterion
def stopCriterionDefault(state, tol):
    if state.nIter == 0:
        return False
    else:
        return state.delta <= tol

def stopCriterionInfNorm(state, tol):
    if state.nIter == 0:
        return False
    else:
        deltaAlternative = state.ea + max(np.abs(state.sa))
        return deltaAlternative <= tol

# prox-param

def proxParamDefault(state, changeProxCenter, context=None):
    xk = state.xSerious
    zkprox = state.xLast
    sk = state.sSerious
    skprox = state.sLast

    if changeProxCenter:
        dx = zkprox - xk
        ds = skprox - sk
        dsds = np.dot(ds,ds)
        if dsds < 1e-5:
            extra = 0
        else:
            extra = np.dot(dx, ds)/dsds
        aux = (1.0/state.muS) + extra
        # print('ds = {}'.format(ds)) #jp: debugging
        # print('muk = {}'.format(muk)) #jp: debugging
        # print('1/mukp = {}'.format(aux)) #jp: debugging
        newMu = 1.0/aux
        state.muS = newMu
        state.muN = newMu
    else:
        state.muN  += 1
        newMu = state.muN
    return newMu

# bundle compression
def bundleCompressProxLMDual(bundle, context=None):
    epz = 1e-4
    
    if context is not None:
        if 'active' in context:
            activeIndex = [i for i, pi  in  bundle.cut[:bundle.size,1] if bundle.alpha[p]<= epz]
            notActiveIndex = [ i for i in range(bundle.maxSize) if i not in activeIndex]
            la = len(activeIndex)
            bundle.size = la
            if la > 0:
                actAux = bundle.cut[activeIndex, :] 
                bundle.cut[la:,:] = bundle.cut[notActiveIndex, :]
                bundle.cut[:la,:] = actAux 
                alphaSum = sum(bundle.alpha[bundle.cut[:la,1]])
                bundle.alpha[:] = bundle.alpha/alphaSum

    dd = bundle.size + 2 - bundle.maxSize

    if dd > 0:
        bundle.size -= dd
        aux = bundle.cut[:dd,:].copy()
        bundle.cut[:-dd,:] = bundle.cut[dd:,:]
        bundle.cut[-dd:, :] = aux
        

#solving the cutting plane model
def solveProxLMDual(state, bundle, mu, context=None):
    xk = state.xSerious
    fk = state.fSerious
    nb = bundle.size
    n = len(xk)
    bIndex = bundle.cut[:nb, 0]

    sa = np.zeros(n)
    ea = 0

    import gurobipy as grb

    m = grb.Model('dual')
    m.setParam('LogtoConsole', 0)
    alpha = m.addVars(bIndex, vtype=grb.GRB.CONTINUOUS, lb=0.0, ub=1.0, name='alpha')
    m.update()

    # Constrain sum alpha ==1
    m.addConstr(grb.quicksum(alpha) == 1, name='sum1')

    # Setting the objective function
    # print(bundle) # JP:  debbuging
    aux = { (idc, pos):bundle.c0[pos] + np.dot(bundle.c1[pos], xk) for idc, pos in bundle.cut[:nb] }
    objF = -grb.quicksum(alpha[idc] * aux[(idc, pos)] for idc, pos in bundle.cut[:nb] )
    objF += grb.quicksum(np.dot(bundle.c1[pi], bundle.c1[pj]) * alpha[i] * alpha[j] for i, pi in bundle.cut[:nb] for j, pj in bundle.cut[:nb]) / (2 * mu)
    m.setObjective(objF, sense=grb.GRB.MINIMIZE)

    # Solving the problem
    # m.write('jj.lp')
    # m.write('jj.mps')
    m.optimize()
    optVal = m.objVal
    proxOptVal = -optVal

    alphak = {}

    for idc, pos in bundle.cut[:nb]:
        alphak[(idc, pos)] = alpha[idc].x
        sa += alpha[idc].x * bundle.c1[pos]
    ea = fk - proxOptVal - (np.dot(sa,sa) / (2 * mu))

    if (ea < 0):
        print('proxLMSolver warning: ea = {} less than zero'.format(ea))
        ea = max([0, ea]) # Just for avoiding negative values of ea

    zkprox = xk - (sa/ mu)
    ca0 = fk - np.dot(sa, xk) - ea
    status  = 0

    return sa, ca0, ea, zkprox, proxOptVal, alphak, status
        

def proxBundleMin(x0, 
        bb, bbContext=None, 
        proxLMSolver=None, proxLMSolverContext=None, 
        bundleManagerFun=None, bundleManagerFunContext=None, 
        stopCriterionFun=None, stopCriterionFunContext=None,
        proxParamFun=None, proxParamFunContext=None,
        bundle=None,
        state=None,
        m=0.5,
        lb=None, ub=None,
        lhs=None, conSense=None, rhs=None,
        context=None, 
        xLog=None, globalLog=None, 
        tol=1e-5, maxIter=10): 

    '''
    def proxBundle(bb, x0, context=None, mu0=1, m=0.5, 
        solveProxLM=None, 
        nullStepParamUpdate=None,
        seriousStepParamUpdate=None,
        bundleCompress=None, bundleCompressContex={},
        xLog=None, globalLog=None, 
        tol=1e-5, maxIter=10): 
 
    bb: non smooth black box
    x0: initial point
    xLog: path file for recording the sequence rengerated by the algorithm
    globalLog: path file for general information 
    maxIter:
    '''

    startTime = time.time()

    if xLog is not None: xLogFile = open(xLog, 'w')
    if globalLog is not None: globalLogFile = open(globalLog, 'w')


    state.xSerious = np.array(x0) #prox center
    xk = state.xSerious
    state.sSerious = np.zeros_like(x0)
    sk = state.sSerious

    state.xLast = np.array(x0) #prox center
    zkprox = state.xLast
    state.sLast = np.zeros_like(x0) #prox center 
    skprox = state.sLast

    state.sa = np.zeros_like(x0) #prox center

    fk, sk[:], bbStatus = bb(xk, mode=2, context=bbContext) # sk: proximal center subgrad
    state.fLast = fk
    state.fSerious = fk

    state.sLast[:] = sk


    bundle.addCut(sk, fk - np.dot(sk, xk)) # addinf the first cut 
    
    mu = proxParamFun(state, changeProxCenter=False, context=proxParamFunContext)

    state.nIter = 0
    seriousK = 0
    nullLog = ''
    # sa = [] #just for using the fist time on stopCriterion
    # ea = 100 #just for using the fist time on stopCriterion
    state.delta= tol  + 1 # ensuring the first iteration

    if xLog is not None: 
        xLogFile.write(','.join(['[k={}][serious={}{}]'.format(state.nIter, seriousK, nullLog)] 
            + [str(val) for val in xk]) + '\n')


    if globalLog is not None: 
        globalLogFile.write('[k={}],[serious={}{}],f(xk)={},f(z k+1)={},delta={}, proxOptVal={}, mu={},bundleSize={}\n'.format(state.nIter, seriousK, nullLog,
                    fk, None, state.delta, None, None, None))
    
    stopOK = stopCriterionFun(state, tol)

    while ((state.nIter < maxIter) and not stopOK):
        state.nIter +=1 
        state.sa[:], state.ca0, state.ea, zkprox[:], proxOptVal, alpha, pmStatus = proxLMSolver(state, bundle, mu, context=proxLMSolverContext)
        state.delta = fk - proxOptVal

        #recording alpha values in bundle.
        for idc, i in alpha:
            bundle.alpha[i] = alpha[(idc, i)]

#         if bundleManagementContext is not None:
            # if 'cutId' in bundleManagementContext:
#                 bundleManagementContext['cutId'] = newBundleId
##JP manage bundle

        # managing bundle the bundle 
        bundleManagerFun(bundle, context=bundleManagerFunContext)


        bundle.addCut(state.sa, state.ca0, agregateCut=True)

        fzkprox, skprox[:], bbStatus = bb(zkprox, mode=2, context=bbContext)
        state.fLast = fzkprox

        bundle.addCut(skprox, fzkprox - np.dot(skprox, zkprox)) # addinf the first cut 

        # Checking if a serious step was reached
        if  fzkprox < fk - m * state.delta: # we have a serious step
            nullLog =  ''
            changeProxCenter = True

            mu = proxParamFun(state, changeProxCenter=changeProxCenter, context=proxParamFunContext)
            
            xk[:] = zkprox
            fk = fzkprox
            state.fSerious = fk
            
            seriousK = state.nIter
        else: # null step
            nullLog = '-null'
            changeProxCenter = False
            # ekprox = fk - (fzkprox + np.dot(skprox, xk - zkprox))
            # bundle.append((newBundleId, (skprox, max([0, ekprox]))))#This newBundleId comes from newBundleId(1)
            mu = proxParamFun(state, changeProxCenter=changeProxCenter, context=proxParamFunContext)

        if xLog is not None: 
            xLogFile.write( ','.join(['[k={}][serious={}{}]'.format(state.nIter, seriousK, nullLog)] 
                + [str(val) for val in zkprox]) + '\n')
        if globalLog is not None: 
            globalLogFile.write('[k={}],[serious={}{}],f(xk)={},f(z k+1)={},delta={},proxOptVal={}, mu={},bundleSize={}\n'.format(
                state.nIter, seriousK, nullLog,fk, fzkprox, state.delta, proxOptVal, mu, bundle.size))

        stopOK = stopCriterionFun(state, tol)

    if xLog is not None: xLogFile.close()
    if globalLog is not None: globalLogFile.close()
    endTime = time.time()
    
    if stopOK:
        status = 0
    elif state.nIter>=maxIter:
        status = 100
    else:
        status = bbStatus

    return common.result_class(x_best=xk, f_best=fk, x_last=zkprox, f_last=fzkprox, deltak=state.delta, nIter=state.nIter, status=status, time=endTime-startTime)


def minimize(x0, 
        fun=None, grad=None, 
        bb=None, bbContext=None, 
        proxLMSolver=None, proxLMSolverContext=None, 
        bundleManagerFun=None, bundleManagerFunContext=None, 
        stopCriterionFun=None, stopCriterionFunContext=None,
        proxParamFun=None, proxParamFunContext=None,
        bundle=None,
        bundleMaxSize=10,
        mu0=1,
        m=0.5,
        lb=None, ub=None,
        lhs=None, conSense=None, rhs=None,
        context=None, 
        xLog=None, globalLog=None, 
        tol=1e-5, maxIter=10): 

    import pynso.tools

    if bb is None:
        if ((fun is None) or (grad is None)):
            print("Error: ('fun' and 'grad') or 'bb' must be provided")
            return common.result_class(status=1)
        else:
            bb = lambda x, mode=0, context=None: pynso.tools.fgBB(x, mode=mode, context=context, fun=fun, grad=grad)

    state = algothmState(mu0)

    if stopCriterionFun is None: 
        stopCriterionFun  = stopCriterionDefault 

    if proxParamFun is None:
        proxParamFun = proxParamDefault 

    if bundleManagerFun is None:
        bundleManagerFun = bundleCompressProxLMDual

    if bundle is None:
        bundle = bundleClass(len(x0), maxSize=bundleMaxSize)

    ## prox solver 
    if proxLMSolver is not None:
        if isinstance(solveProxM, str): 
            print('Using the dual solver. Currently it is the only option ') 
            proxLMSolver = solveProxLMDual
    else: 
        proxLMSolver = solveProxLMDual



    result = proxBundleMin(x0, 
            bb=bb,
            bbContext=bbContext,
            proxLMSolver=proxLMSolver,
            proxLMSolverContext=proxLMSolverContext,
            bundleManagerFun=bundleManagerFun,
            bundleManagerFunContext=bundleManagerFunContext,
            stopCriterionFun=stopCriterionFun,
            stopCriterionFunContext=stopCriterionFunContext,
            proxParamFun=proxParamFun,
            proxParamFunContext=proxParamFunContext,
            bundle=bundle,
            state=state,
            m=m,
            lb=lb,
            ub=ub,
            lhs=lhs,
            conSense=conSense,
            rhs=rhs,
            context=context,
            xLog=xLog,
            globalLog=globalLog,
            tol=tol,
            maxIter=maxIter)



    return result
