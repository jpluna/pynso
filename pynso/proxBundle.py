import numpy as np
import pynso.common as common



def minimize(x0, fun=None, grad=None, bb=None, bbContext=None, fg=None, fgContext=None,
        solveProxM=None, solveProxMConxtext=None,
        proxParam=None, proxParamContext=None,
        proxParamFun=None, proxParamFunContext=None,
        bundleManagerFun=None, bundleManagerContext=None,
        bundle=None,
        bundleMaxSize=5,
        mu0=1,#for python version
        m=0.5,
        lb=None,
        ub=None,
        LHS=None,
        sense=None,
        RHS=None,
        context=None, 
        xLog=None, globalLog=None, Log=None,
        stopCriterion=None, 
        stopCriterionFunContext=None,
        outputMessageLevel=4,#bmrio
        bmrioAlg=1,#bmrio
        inexactError=0,#bmrio
        targetValue=None,
        optimalValue=None,
        tol=1e-5, maxIter=10, method='python'):
    import pynso.tools
    import pynso.pyProxBundle as pypb
    import pynso.pyProxBundle2 as pypb2
    import pynso.pyProxBundleNew as pypbNew

    if ((fg is not None) and (bb is None)): ## JP: temporal hack
        bb = lambda x, mode=0, context=None: pynso.tools.fgBB(x, mode=mode, context=context, fg=fg)

    
    if method.lower() == 'bmrio': 
        try:
            import pybmrio as bmrio
            
            x_best, f_best, x_last, f_last, deltak, nIter, time, status = bmrio.minimize(x0, 
                    fun=fun,
                    grad=grad,
                    bb=bb,
                    bbContext=bbContext,
                    bundleMaxSize=bundleMaxSize,#py
                    lb=lb,#py
                    ub=ub,#py 
                    outputMessageLevel=outputMessageLevel,
                    bmrioAlg=bmrioAlg,
                    inexactError=inexactError,
                    globalLog=globalLog,
                    tol=tol,
                    maxIter=maxIter
                    ) 
            result = common.result_class(x_best=x_best, f_best=f_best, x_last=x_last, f_last=f_last, deltak=deltak, nIter=nIter, time=time, status=status)

        except:
            # print('pybmrio module not found')
            raise RuntimeError('pybmrio module not found')
    elif method.lower() == 'pythonv2':
        result = pypb2.minimize(x0, 
                fun=fun,
                grad=grad,
                bb=bb,
                bbContext=bbContext, 
                proxLMSolver= solveProxM, 
                proxLMSolverContext=solveProxMConxtext, 
                proxParamFun= proxParam, 
                proxParamFunContext=proxParamContext, 
                bundleManagerFun= bundleManagerFun, 
                bundleManagerFunContext=bundleManagerContext, 
                stopCriterionFun= stopCriterion, 
                stopCriterionFunContext=stopCriterionFunContext,
                bundle=bundle, 
                bundleMaxSize=bundleMaxSize,
                mu0=mu0,
                m=m,
                context=context,
                xLog=xLog,
                globalLog=globalLog,
                tol=tol,
                maxIter=maxIter
                )

    elif method.lower() == 'pnew':

        result = pypbNew.minimize(x0, 
                fun=fun,
                grad=grad,
                bb=bb,
                bbContext=bbContext, 
                proxLMSolver= solveProxM, 
                proxLMSolverContext=solveProxMConxtext, 
                proxParamFun= proxParamFun, 
                proxParamFunContext=proxParamContext, 
                bundleManagerFun= bundleManagerFun, 
                bundleManagerFunContext=bundleManagerContext, 
                stopCriterionFun= stopCriterion, 
                stopCriterionFunContext=stopCriterionFunContext,
                bundle=bundle, 
                bundleMaxSize=bundleMaxSize,
                mu0=mu0,
                m=m,
                xLog=xLog, globalLog=globalLog, Log=Log,
                context=context,
                tol=tol,
                maxIter=maxIter
                )
    else:
        result = pypb.minimize(x0, 
                fun=fun,
                grad=grad,
                bb=bb,
                bbContext=bbContext,
                solveProxM=solveProxM,
                solveProxMConxtext=solveProxMConxtext,
                proxParam=proxParam,
                proxParamContext=proxParamContext,
                bundleManagerFun=bundleManagerFun,
                bundleManagerContext=bundleManagerContext,
                bundle=bundle,
                mu0=mu0,
                m=m,
                context=context,
                xLog=xLog,
                globalLog=globalLog,
                tol=tol,
                maxIter=maxIter
                )

    return result
